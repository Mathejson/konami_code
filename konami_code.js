let input = '';
let time;
const onKonamiCode = () => {
    const key = 'injects3crets';
    document.addEventListener('keydown', function (e) {
        if (e.key === 'Escape')
            input = '';
        else {
            input += ('' + e.key);
            if (input === key) {
                fetchData();
                input='';
            }
        }
        console.log(input);
    });
    if (input === key) {
        fetchData();
        input='';
    }
    inactivityTime(input);

};

const inactivityTime  = () => {
    let time;
    document.onkeypress = resetTimer;

    function clearInput() {
        input = '';
    }

    function resetTimer() {
        clearTimeout(time);
        time = setTimeout(clearInput, 5000)
    }
};
const fetchData = () => {
    document.getElementById("data").innerHTML = '';
    let data = fetch('https://api.github.com/repos/elixir-lang/elixir/issues')
        .then(res => res.json());
    data.then((result)=> {
        const sorted = result.sort((a,b)=>{
            a.created_at.localeCompare(b.date);
        });
        let newest = sorted.slice(0, 5);
        newest.forEach(item=>{
            let paragraph = document.getElementById("data");
            paragraph.innerHTML += `name: ${item.user.login} title: ${item.title} <br>`;
        });
    });
    clearTimeout(time);
    time = setTimeout(()=> { document.getElementById("data").innerHTML = ""; }, 15000);
};
onKonamiCode();
